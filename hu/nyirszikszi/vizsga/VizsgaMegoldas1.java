package hu.nyirszikszi.vizsga;



public class VizsgaMegoldas1 {

    private int oldal1;
    private int oldal2;
    private int oldal3;
    
    public VizsgaMegoldas1(int o1, int o2, int o3){
        this.oldal1 = o1;
        this.oldal2 = o2;
        this.oldal3 = o3;
    }
    
    public int getKerulet(){
        return this.oldal1 + this.oldal2 + this.oldal3;
    }
    
    
}
