package hu.nyirszikszi.vizsga;


public class VizsgaMegoldas2 {

    int irany;
    
    public VizsgaMegoldas2(int irany){
        if(irany >= 0 && irany <= 3)
            this.irany = irany;
        else
            this.irany = 0;
    }

    // egyet fordul balra
    public void balraFordulEgyet(){
        irany--;
        if(irany < 0)
            irany = 3;
    }
    
    // egyet fordul jobbra
    public void jobbraFordulEgyet(){
        irany++;
        if(irany > 3)
            irany = 0;
    }
    
    // hanyszor paraméter értékével fordul balra
    public void balraFordulTobbet(int hanyszor){
        for(int i = 1; i <= hanyszor; i++){
            irany--;
            if(irany < 0)
                irany = 3;
            System.out.println("A balra fordulás száma: " + i + ". Irány " + this.getIrany() + ".");
        }
    }
    
    // hanyszor paraméter értékével fordul jobbra
    public void jobbraFordulTobbet(int hanyszor){
        for(int i = 1; i <= hanyszor; i++){
            irany++;
            if(irany > 3)
                irany = 0;
            System.out.println("A jobbra fordulás száma: " + i + ". Irány " + this.getIrany() + ".");
        }
    }
    
    // visszaadja az aktuális irányt szövegesen
    public String getIrany(){
        String iranySzöveggel = "";
        switch(this.irany){
            case 0:
                iranySzöveggel = "észak";
                break;
            case 1:
                iranySzöveggel = "kelet";
                break;
            case 2:
                iranySzöveggel = "dél";
                break;
            case 3:
                iranySzöveggel = "nyugat";
                break;
        }
        return iranySzöveggel;
    }
    
    
}
