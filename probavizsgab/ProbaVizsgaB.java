package probavizsgab;

import hu.nyirszikszi.vizsga.VizsgaMegoldas1;
import hu.nyirszikszi.vizsga.VizsgaMegoldas2;


public class ProbaVizsgaB {


    public static void main(String[] args) {

        VizsgaMegoldas1 vm1 = new VizsgaMegoldas1(3, 5, 4);
        System.out.println("A háromszög kerülete: " + vm1.getKerulet());

        System.out.println("-------------");
        
        
        VizsgaMegoldas2 vm2 = new VizsgaMegoldas2(0);
        
        System.out.println("Az alap irány " + vm2.getIrany());
        
        /**
         * Minden egyes fordulásnál meg kell hívni a fordulást végző függvényt
         */
        vm2.balraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.balraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.balraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.balraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.balraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.jobbraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.jobbraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.jobbraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        vm2.jobbraFordulEgyet();
        System.out.println("Az irány " + vm2.getIrany());
        
        System.out.println("-------------");
        
        /**
         * Egyszer hívjuk meg a függvényt, és paraméterben átadjuk
         * a fordulások számát.
         */
        vm2.balraFordulTobbet(5);
        vm2.jobbraFordulTobbet(8);
        
    }
    
}
